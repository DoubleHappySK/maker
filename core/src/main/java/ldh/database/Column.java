package ldh.database;

import ldh.database.util.JavaMapJdbc;
import ldh.database.util.JdbcUtil;
import ldh.maker.util.FreeMakerUtil;

public class Column {

	private String name;
	private String type; //数据类型
	private String property;
	private Class<?> propertyClass;
	private Class<?> defaultPropertyClass;
	private WapClass wapClass;
	private boolean isWap = false;
    private String javaType;
	private String comment; //注释
	private String text; // 文本内容
	private int size; //列的长度
	private boolean nullable; ///是否为空
	private int scale;  //精度空
	private Integer width;

	//是否是外键
	private boolean foreign;

	//是否是外键
	private boolean oneToOne;

	private boolean isCreate = true;

	private boolean isUnique; //是否唯一

	private ForeignKey foreignKey;

	public Column(String name, String comment) {
		this(name, comment, null);
	}

	public Column(String name, String comment, String type) {
		this.name = name;
		this.comment = comment;
		this.type = type;
		this.text = comment;
		property(type);
	}

	private void property(String type) {
		if (type == null) return;
		JavaMapJdbc jmj = JdbcUtil.getJavaMapJdbc(type);
		if (jmj == null) throw new RuntimeException("找不到类:" + type);
		propertyClass = jmj.getClazz();
		defaultPropertyClass = propertyClass;
	}

	//定义java驼峰式名称
	private void handle() {
		String columnName = name.toLowerCase();
		property = FreeMakerUtil.javaName(columnName);
		if (this.isForeign()) {
			if (columnName.endsWith("_id") || columnName.endsWith("_ID")) {
				columnName = columnName.substring(0, columnName.length() - 3);
			}
			property = FreeMakerUtil.javaName(columnName);
			wapClass = new WapClass(property, WapClass.WapType.TABLE);
		}
	}

	public void setType(String columnType) {
		property(columnType);
	}

	public String getName() {
		return name;
	}

	public String getProperty() {
		if (property == null) {
			handle();
		}
		return property;
	}

	public Class<?> getPropertyClass() {
		return propertyClass;
	}

	public String getType() {
		return type;
	}

	public String getJavaType() {
	    if (propertyClass == Enum.class && javaType != null) {
            return javaType;
        }
		return propertyClass.getSimpleName();
	}

	public String getFullJavaType() {
		return propertyClass.getName();
	}

	public void setJavaType(Class<?> javaType) {
		propertyClass = javaType;
	}

    public void setJavaType(String javaType) {
        this.javaType = javaType;
    }

	public String getComment() {
		if (comment == null || comment.equals("") || comment.equals(name)) {
			return null;
		}
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public boolean isForeign() {
		return foreign;
	}

	public void setForeign(boolean foreign) {
		this.foreign = foreign;
	}

	public boolean isCreate() {
		return isCreate;
	}

	public void setCreate(boolean isCreate) {
		this.isCreate = isCreate;
	}

	public ForeignKey getForeignKey() {
		return foreignKey;
	}

	public void setForeignKey(ForeignKey foreignKey) {
		this.foreignKey = foreignKey;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public boolean isNullable() {
		return nullable;
	}

	public void setNullable(boolean nullable) {
		this.nullable = nullable;
	}

	public int getScale() {
		return scale;
	}

	public void setScale(int scale) {
		this.scale = scale;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public boolean isUnique() {
		return isUnique;
	}

	public void setUnique(boolean isUnique) {
		this.isUnique = isUnique;
	}

	public boolean isOneToOne() {
		return oneToOne;
	}

	public void setOneToOne(boolean oneToOne) {
		this.oneToOne = oneToOne;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public WapClass getWapClass() {
		return wapClass;
	}

	public void setWapClass(WapClass wapClass) {
		this.wapClass = wapClass;
	}

	public boolean isWap() {
		return isWap;
	}

	public void setWap(boolean wap) {
		isWap = wap;
	}

	public Class<?> getDefaultPropertyClass() {
		return defaultPropertyClass;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!(obj instanceof Column)) {
			return false;
		}
		Column c = (Column) obj;
		if (this.getName().equals(c.getName())) {
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.getName().hashCode();
	}
}
