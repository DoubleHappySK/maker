package ldh.maker.vo;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ldh on 2017/2/25.
 */
public class DBConnectionData {

    private StringProperty nameProperty = new SimpleStringProperty();
    private StringProperty ipProperty = new SimpleStringProperty();
    private IntegerProperty portProperty = new SimpleIntegerProperty();
    private StringProperty userNameProperty = new SimpleStringProperty();
    private StringProperty passwordProperty = new SimpleStringProperty();
    private TreeNode treeNode;
    private Connection connection;

    public String getNameProperty() {
        return nameProperty.get();
    }

    public StringProperty namePropertyProperty() {
        return nameProperty;
    }

    public void setNameProperty(String nameProperty) {
        this.nameProperty.set(nameProperty);
    }

    public String getIpProperty() {
        return ipProperty.get();
    }

    public StringProperty ipPropertyProperty() {
        return ipProperty;
    }

    public void setIpProperty(String ipProperty) {
        this.ipProperty.set(ipProperty);
    }

    public Integer getPortProperty() {
        return portProperty.get();
    }

    public IntegerProperty portPropertyProperty() {
        return portProperty;
    }

    public void setPortProperty(Integer portProperty) {
        this.portProperty.set(portProperty);
    }

    public String getUserNameProperty() {
        return userNameProperty.get();
    }

    public StringProperty userNamePropertyProperty() {
        return userNameProperty;
    }

    public void setUserNameProperty(String userNameProperty) {
        this.userNameProperty.set(userNameProperty);
    }

    public String getPasswordProperty() {
        return passwordProperty.get();
    }

    public StringProperty passwordPropertyProperty() {
        return passwordProperty;
    }

    public void setPasswordProperty(String passwordProperty) {
        this.passwordProperty.set(passwordProperty);
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public TreeNode getTreeNode() {
        return treeNode;
    }

    public void setTreeNode(TreeNode treeNode) {
        this.treeNode = treeNode;
    }

    @Override
    public String toString() {
        return nameProperty.get();
    }
}