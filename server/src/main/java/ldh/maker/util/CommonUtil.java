package ldh.maker.util;

/**
 * Created by ldh on 2017/4/19.
 */
public class CommonUtil {

    public static Object to(String value, String type) {
        try {
            String classType = getValue(type);
            if (classType.equals("int")) {
                return Integer.valueOf(value);
            } else if (classType.equals("short")){
                return Short.valueOf(value);
            } else if (classType.equals("byte")){
                return Byte.valueOf(value);
            } else if (classType.equals("boolean")){
                if (value.equals("0")) return false;
                if (value.equals("1")) return true;
                return Boolean.valueOf(value);
            }
        } catch (Exception e) {

        }
        return null;
    }

    public static String getValue(String str) {
        int idx1 = str.indexOf("(");
        int idx2 = str.indexOf(")");
        String value = str.substring(idx1+1, idx2);
        return value;
    }

    public static String getName(String str) {
        int idx = str.indexOf("(");
        String value = str.substring(0, idx);
        return value;
    }

    public static String getDesc(String str) {
        int idx1 = str.indexOf("--");
        String value = str.substring(idx1+2);
        return value;
    }
}
