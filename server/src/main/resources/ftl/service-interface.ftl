package ${package};

<#if imports?exists>
	<#list imports as import>
import ${import};
	</#list>
</#if>

public interface ${className} <#if implements?exists>extends <#list implements as implement>${implement}<#if implement_has_next>,</#if></#list></#if> ${r'{'}
	
	<#------------------insert--------------------------------------------------------------------------------------------->
	<#if util.isCreate(table, "insert") >
	${bean} insert(${bean} ${util.javaName(bean)});
	
	</#if>
	<#------------------update--------------------------------------------------------------------------------------------->
	<#list table.indexies as index>
	<#if index.primaryKey>
	<#if util.isCreate(table, "updateBy${util.uniqueName(index)}") >
	${bean} updateBy${util.uniqueName(index)}(${bean} ${util.javaName(bean)});

	</#if>
	<#------------------delete------------------------------------------------------------------------------------------->
	<#if util.isCreate(table, "deleteBy${util.uniqueName(index)}") >
	void deleteBy${util.uniqueName(index)}(${util.uniqueParam(index)});

	</#if>
	</#if>
	</#list>
	<#-----------------------------------getUnique----------------------------------------------------------------------------------->
	<#list table.indexies as index>	
	<#if util.isUnique(index, table)>
	<#if util.isCreate(table, "getBy" + util.uniqueName(index)) >
	${bean} getBy${util.uniqueName(index)}(${util.uniqueParam(index)});
    
    </#if>
    </#if>
	</#list>
	<#------------------getManyToMany------------------------------------------------------------------------------------>
	<#if table.manyToManys??>
	<#list table.manyToManys as mm>	
	<#if util.isCreate(mm.primaryTable, "getMany" + util.firstUpper(mm.secondTable.javaName) + "sById") >
	${bean} getMany${util.firstUpper(mm.secondTable.javaName)}sById(${key} id);
	
	</#if>
	</#list>
	</#if>
	<#------------------getWith------------------------------------------------------------------------------------------->
	<#list table.foreignKeys as foreignKey>	
	<#if util.isCreate(table, "getWith${util.firstUpper(foreignKey.column.property)}ById") >
	${bean} getWith${util.firstUpper(foreignKey.column.property)}ById(${key} id);
	
	</#if>
	</#list>
	<#------------------getWithAssociatesById---------------------------------------------------------------------------->
	<#if table.foreignKeys?size gt 1>
	<#if util.isCreate(table, "getWithAssociatesById") >
	${bean} getWithAssociatesById(${key} id);
	
	</#if>
	</#if>
	<#------------------getJoin------------------------------------------------------------------------------------------>
	<#list table.foreignKeys as foreignKey>	
	<#if util.isCreate(table, "getJoin${util.firstUpper(foreignKey.column.property)}ById") >
	${bean} getJoin${util.firstUpper(foreignKey.column.property)}ById(${key} id);
	
	</#if>
	</#list>
	<#------------------getJoinAssociatesById---------------------------------------------------------------------------->
	<#if table.foreignKeys?size gt 1>
	<#if util.isCreate(table, "getJoinAssociatesById") >
	${bean} getJoinAssociatesById(${key} id);
	
	</#if>
	</#if>
	<#------------------getManyJoin-------------------------------------------------------------------------------------->
	<#list table.many as foreignKey>
	<#if util.isCreate(table, "getJoin${util.firstUpper(foreignKey.table.javaName)}ById") >
	${bean} getJoin${util.firstUpper(foreignKey.table.javaName)}ById(${key} id);
	
	</#if>
	</#list>
	<#------------------getManyJoinAllById------------------------------------------------------------------------------->
	<#if table.many?size gt 1>
	<#if util.isCreate(table, "getJoinAllById") >
	${bean} getJoinAllById(${key} id);
	
	</#if>
	</#if>
	<#------------------find--------------------------------------------------------------------------------------------->
	<#if util.isCreate(table, "find") >
	PageResult<${bean}> findBy${beanWhere}(${beanWhere} ${util.javaName(beanWhere)});
	
	</#if>
	<#------------------findByJoin--------------------------------------------------------------------------------------->
	<#if util.isCreate(table, "findByJoin") >
	<#if table.foreignKeys?? && table.foreignKeys?size gt 0 >
	<#if table.foreignKeys?? && table.foreignKeys?size gt 0 >
	PageResult<${bean}> findJoinBy${beanWhere}(${beanWhere} ${util.javaName(beanWhere)});
	</#if>
	</#if>
	</#if>
${r'}'}